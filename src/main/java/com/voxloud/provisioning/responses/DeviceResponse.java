package com.voxloud.provisioning.responses;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This is a Response Class that wraps combined response from DB and Configured
 * Properties
 * 
 * @author RakhuNathan
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceResponse {

	private String username;

	private String password;

	private String domain;

	private String port;

	private String codecs;

	private String timeout;
}
