package com.voxloud.provisioning.service;

import org.springframework.http.ResponseEntity;

public interface ProvisioningService {
	/**
	 * Fetches provision file with respect to the given mac address
	 * 
	 * @param macAddress - Mac of device whose configuration is supposed to be
	 *                   fetched
	 * @return Returns response as file in byte array
	 */
	ResponseEntity<byte[]> getProvisioningFile(String macAddress);

}
