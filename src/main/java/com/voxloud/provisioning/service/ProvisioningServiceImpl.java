package com.voxloud.provisioning.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import com.voxloud.provisioning.responses.DeviceResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProvisioningServiceImpl implements ProvisioningService {

	@Autowired
	Environment environment;

	@Autowired
	DeviceRepository deviceRepository;

	@Override
	public ResponseEntity<byte[]> getProvisioningFile(String macAddress) {
		Device thisDevice = deviceRepository.findByMacAddressIgnoreCase(macAddress);
		try {
			if (thisDevice != null) {
				String[] fragments = splitFragments(thisDevice);
				return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).body(new Gson().toJson(new DeviceResponse(thisDevice.getUsername(), thisDevice.getPassword(), environment.getProperty("provisioning.domain"),
						environment.getProperty("provisioning.port"), environment.getProperty("provisioning.codecs"), fragments != null && fragments.length > 0 ? fragments[fragments.length - 1] : null)).getBytes());
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Splits the Overridden Fragments into pieces and fetches the required timeout
	 * parameter
	 * 
	 * @param thisDevice
	 * @return
	 */
	private String[] splitFragments(Device thisDevice) {
		if (thisDevice.getOverrideFragment() != null && !thisDevice.getOverrideFragment().isBlank()) {
			if (thisDevice.getOverrideFragment().contains(System.getProperty("line.separator"))) {
				String[] splitData = thisDevice.getOverrideFragment().split(System.getProperty("line.separator"));
				return splitData[splitData.length - 1].split("=");
			} else {
				String splitData[] = { "timeout", new Gson().fromJson(thisDevice.getOverrideFragment(), HashMap.class).get("timeout").toString() };
				return splitData;
			}
		}
		return null;
	}
}
