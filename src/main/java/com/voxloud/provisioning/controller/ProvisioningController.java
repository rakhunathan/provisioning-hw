package com.voxloud.provisioning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voxloud.provisioning.service.ProvisioningService;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

	@Autowired
	ProvisioningService provisioningService;

	@GetMapping(value = "provisioning/{mac}")
	public ResponseEntity<byte[]> getProvisionedDeviceConfiguration(@PathVariable String mac) {

		return provisioningService.getProvisioningFile(mac);

	}
}